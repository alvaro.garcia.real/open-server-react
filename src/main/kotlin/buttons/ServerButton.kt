package buttons

import buttons.Action.*
import http.client.ServiceSwitcher
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.css.Color
import kotlinx.css.backgroundColor
import kotlinx.html.js.onClickFunction
import org.w3c.dom.events.Event
import react.RBuilder
import react.RComponent
import react.RState
import react.ReactElement
import styled.css
import styled.styledButton

class ServerButton : RComponent<ButtonServiceProps, RState>() {
    override fun RBuilder.render() {
        val service = props.service
        val serviceSwitcher = ServiceSwitcher()
        styledButton {
            css {
                when(props.action)
                {
                    ENCENDER -> backgroundColor = Color.green
                    APAGAR -> backgroundColor = Color.red
                }
            }
            attrs {
                onClickFunction = doOnClick(serviceSwitcher, service)
            }
            +props.action.name
        }
    }

    private fun doOnClick(serviceSwitcher: ServiceSwitcher, service: String): (Event) -> Unit {
        return {
            when (props.action) {
                ENCENDER -> MainScope().launch { serviceSwitcher.turnOnComputer(service) }
                APAGAR -> MainScope().launch { serviceSwitcher.turnOffComputer(service) }
                else -> Unit
            }
        }
    }
}

fun RBuilder.serverButton(handler: ButtonServiceProps.() -> Unit) : ReactElement {
    return child(ServerButton::class)
    {
        this.attrs(handler)
    }
}
