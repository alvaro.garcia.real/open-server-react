package buttons

import react.RProps

interface ButtonServiceProps : RProps {
    var service : String
    var action : Enum<Action>
}

enum class Action
{
    ENCENDER, APAGAR
}
