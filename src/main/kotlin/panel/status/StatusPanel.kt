package panel.status

import http.client.ServiceStatus
import kotlinx.browser.window
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import panel.service.ServicePanelProps
import react.*
import react.dom.div
import react.dom.h2
import react.dom.h3


private val serviceStatus = ServiceStatus()

val statusPanel = functionalComponent<ServicePanelProps> { props ->
    val (state, setState) = useState("APAGADO")
    useEffect(listOf(state)) {
        Timeouts.futureTimeouts.forEach { window.clearTimeout(it) }
        console.log("Effect ejecutado")
        Timeouts.futureTimeouts.add(window.setTimeout(handler = { setState("$state   (Comprobando)") }, timeout = 20000))
    }
    div {
        h2 {
            "Servicio ${props.service}"
        }
        h3 {
            GlobalScope.launch {
                val serverStatus = serviceStatus.status(props.service).status
                setState("Estado actual ${if(serverStatus) "ENCENDIDO Y FUNCIONANDO" else "APAGADO" }")
            }
            +state
        }
    }
}

object Timeouts
{
    val futureTimeouts = mutableListOf<Int>()
}

fun RBuilder.statusPanel(handler: ServicePanelProps.() -> Unit) = child(statusPanel)
{
    attrs{handler()}
}
