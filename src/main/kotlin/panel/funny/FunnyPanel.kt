package panel.funny

import http.client.ServiceStatus
import kotlinx.browser.window
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import panel.service.ServicePanelProps
import react.*
import react.dom.div
import react.dom.h2
import react.dom.h3

val funnyPanel = functionalComponent<RProps> { props ->
    val (state, setState) = useState(0)
    useEffect(listOf(state)) {
        window.setTimeout(handler = { setState((state + 1) % 100) }, timeout = 10000)
    }
    div {
        h3 {
            +"Entra al servidor, tu tarea hoy es ${getRandomPhrase()}"
        }
    }
}

fun getRandomPhrase() = listOf(
    "cocinar kebabs",
    "colocar carteles",
    "construir carreteras",
    "morir en barcos",
    "plantar zanahorias",
    "cavar pozos de sida",
    "comerte un kebab en Nuevo Kebab",
    "picar cobre",
    "que te mate un mosquito",
    "caerte por la borda",
    "recoger las cosas de tu lápida",
    "construir puentes",
    "romper el salto de altura libre",
    "atravesar un portal"
).random()

fun RBuilder.funnyPanel(handler: RProps.() -> Unit) = child(funnyPanel)
{
    attrs{handler()}
}
