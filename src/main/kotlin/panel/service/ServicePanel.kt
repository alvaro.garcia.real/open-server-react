package panel.service

import buttons.Action
import buttons.serverButton
import react.RBuilder
import react.RComponent
import react.RState
import react.dom.div
import react.dom.h2

class ServicePanel : RComponent<ServicePanelProps, RState>() {
    override fun RBuilder.render() {
        div {
            h2 { props.service }
            +props.service
            serverButton {
                service = props.service
                action = Action.ENCENDER
            }
            serverButton {
                service = props.service
                action = Action.APAGAR
            }
        }
    }
}

fun RBuilder.servicePanel(handler : ServicePanelProps.() -> Unit) = child(ServicePanel::class)
{ this.attrs(handler) }
