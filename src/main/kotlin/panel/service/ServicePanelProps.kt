package panel.service

import react.RProps

external interface ServicePanelProps : RProps {
    var service : String
}
