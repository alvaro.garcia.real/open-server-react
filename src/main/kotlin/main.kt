import kotlinx.browser.document
import kotlinx.css.*
import panel.funny.funnyPanel
import panel.service.servicePanel
import panel.status.statusPanel
import react.dom.h1
import react.dom.h2
import react.dom.render
import styled.css
import styled.styledDiv

fun main() {
    render(document.getElementById("root")) {
        styledDiv {
            css {
                backgroundColor = Color("#e8e8e8")
                justifyContent = JustifyContent.flexStart
                flexDirection = FlexDirection.column
            }
            h1 {
                +"Servidores Manines"
            }
            h2 {
                +"¿Qué es lo que quieres hacer hoy?"
            }
            servicePanel {
                service = "valheim"
            }
            statusPanel {
                service = "valheim"
            }
            servicePanel {
                service = "masterrace"
            }
            statusPanel {
                service = "masterrace"
            }
            funnyPanel {  }
        }
    }
}
