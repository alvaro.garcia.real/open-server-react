package http.client

import kotlinx.browser.window
import kotlinx.coroutines.await


class ServiceStatus {
    suspend fun status(service: String): ServiceResult {
        return try {
            val responsePromise = window.fetch("$service/status")
            val response = responsePromise.await()
            val jsonPromise = response.json()
            val json = jsonPromise.await()
            json.unsafeCast<ServiceResult>()
        } catch (ex : dynamic) {
            FailedResult()
        }
    }

    interface ServiceResult
    {
        val status : Boolean
    }

    class FailedResult : ServiceResult
    {
        override val status = false

    }
}


