package http.client

import kotlinx.browser.window
import kotlinx.coroutines.await


class ServiceSwitcher {
    suspend fun turnOnComputer(service: String) = switch(service, "on")

    suspend fun turnOffComputer(service: String) = switch(service, "off")

    private suspend fun switch(service: String, action: String): SwitchResult {
        val responsePromise = window.fetch("$service/$action")
        val response = responsePromise.await()
        val jsonPromise = response.json()
        val json = jsonPromise.await()
        return json.unsafeCast<SwitchResult>()
    }

    interface SwitchResult
    {
        val status : Boolean
    }
}


